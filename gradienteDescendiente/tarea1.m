% Cargar datos
x = load('ex2x.dat');
y = load('ex2y.dat');
% Grafico los datos de entrada

m= length(y);
X=[ones(m,1) x];
MAX_ITER=500;
alpha=0.1;
W=zeros(size(X(1,:)))';
J=[];
costo=0.0; % Costo.
for num_iterations=1:MAX_ITER
	for i=1:m
		sum=(W(1)*X(i,1)+W(2)*X(i,2))-y(i);
		w0_cpy=W(1)-(alpha*(1/m)*sum)*X(i,1);
		w1_cpy=W(2)-(alpha*(1/m)*sum)*X(i,2);

		W(1)=w0_cpy;
		W(2)=w1_cpy;
	end

	% Funcion de Costo
	temp=0.0;
	for i=1:m
		temp=temp+((W(1)*X(i,1)+W(2)*X(i,2))-y(i))^2;
	end
	J=[J (1/(2*m))*temp];
	costo=(1/(2*m))*temp;
end

% Recta 
Y=[ones(m,1)];
for i=1:m
	Y(i)=W(1)*X(i,1)+W(2)*X(i,2);
end;

plot(x,y,'o');
hold on

fprintf('Program paused. Press enter to continue.\n');
pause;

%plot(X(:,2),(W(1)*X(:,1)+W(2)*X(:,2)),'r');
plot(X(:,2),Y,'r');

fprintf('Program paused. Press enter to continue.\n');
pause;

hold off
%Plot costo
plot(J,'r');
ylabel('J(w)');
disp('Ultimo valor de la función de costo: '), disp(costo);

